component accessors="true" {
	property name="wirebox" inject="wirebox" persistent="false";

	property name="bcrypt" inject="@BCrypt" persistent="false";

	property name="id";
	property name="email";
	property name="password";
	property name="permissions";

	public User function setPassword( required string password ){
		return assignAttribute( "password", bcrypt.hashPassword( arguments.password ) );
	}

	public boolean function hasPermission( required string permission ){
		query = wirebox.getInstance('QueryBuilder@qb');
		q = query.from('users').select('permissions').whereID( this.getID() ).get();
		if ( arrayLen(q) and listFind(q[1].permissions, arguments.permission) gt 0) {
			return true;
		} else {
			return false;
		}

	}

	public boolean function isValidCredentials( required string email, required string password ){
		var user = newEntity().where( "email", arguments.email ).first();
		if ( isNull( user ) ) {
			return false;
		}
		return bcrypt.checkPassword( arguments.password, user.getPassword() );
	}

	public User function retrieveUserByUsername( required string email ){
		return newEntity().where( "email", arguments.email ).firstOrFail();
	}

	public User function retrieveUserById( required numeric id ){
		return newEntity().findOrFail( arguments.id );
	}

	public struct function getMemento(){
		return { "email" : variables.getEmail() };
	}

}
